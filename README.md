# Desafio NodeJS Backend

Bem-vindo/Bem-vinda ao desafio NodeJS backend júnior da agil.net!

![](/docs/welcome.gif)

O desafio contempla uma API responsável por administrar uma lista de tarefas do usuário, nenhuma interface deve ser desenvolvida.

- Como um usuário eu desejo cadastrar uma tarefa na minha lista de tarefas
  - A aplicação deve salvar a data e a hora de criação
  - Quero informar descrição e prazo da tarefa
- Como um usuário eu desejo editar uma tarefa marcando-a como finalizada
  - A aplicação deve salvar a data e a hora de quando a tarefa foi finalizada
- Como um usuário eu desejo editar uma tarefa
  - A aplicação deve salvar a data e a hora de edição
  - Quero poder atualizar descrição e/ou prazo
  - Uma tarefa concluída não pode ser atualizada
- Como um usuário eu desejo poder listar todas as minhas tarefas
  - A lista deve ser paginada, podendo escolher quantos itens por vez e se desejo ir para frente ou para trás na lista de itens
  - A aplicação deve notificar através de um campo booleano se a tarefa está ou não atrasada
  - A listagem deve conter todos os dados de cada tarefa

## Requisitos Técnicos

![](/docs/api.gif)

### Obrigatórios

- Código em inglês
- Usar TypeScript
- Usar NodeJS
- Disponibilizar documentação suficiente para a execução do projeto no README

### Diferenciais

- Escrever testes de unidade e integração
- Documentar as decisões tomadas durante o desafio técnico
- Utilizar Docker
- Arquitetura REST API
- Tratar erros
- Utilizar banco de dados ou arquivo (JSON, CSV, XML, etc.) para manter os dados persistentes mesmo que a aplicação reinicie

## Avaliação

O objetivo principal deste desafio é avaliar as capacidades do candidato em:

- Uso correto e apropriado das camadas de arquitetura do projeto
- Escrever código limpo
- Estruturar e armazenar dados de forma segura e íntegra

## Orientações

Envie o link do seu repositório do desafio no GitHub para `cto@agil.net`, assim que for recebido você será notificado e daremos feedbacks conforme as avaliações que forem feitas.
